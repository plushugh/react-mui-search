import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles/";
import { CssBaseline } from "@material-ui/core";
import { blue, indigo } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import SearchIcon from "@material-ui/icons/Search";
import Grid from "@material-ui/core/Grid";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Tooltip from "@material-ui/core/Tooltip";
import ReCAPTCHA from "react-google-recaptcha";
import "./App.scss";

function App() {
    const darkTheme = createMuiTheme({
        palette: {
            type: "dark",
            primary: blue,
            secondary: indigo,
        },
    });
    const useStyles = makeStyles((theme) => ({
        formControl: {
            margin: theme.spacing(3),
        },
    }));
    const classes = useStyles();
    const [searchContent, setSearchContent] = useState("");
    const [searchEngine, setSearchEngine] = useState("Google");
    const [open, setOpen] = React.useState(false);
    let captchaStatus = false;
    const changeCaptchaStatus = (value) => {
        console.log(value);
        captchaStatus = true;
    };
    const handleOpenAlert = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleChangeEngine = (event) => {
        setSearchEngine(event.target.value);
    };
    const onSearch = () => {
        if (captchaStatus) {
            switch (searchEngine) {
                case "Google":
                    window.open("https://google.com/search?q=" + searchContent);
                    break;
                case "Bing":
                    window.open("https://bing.com/search?q=" + searchContent);
                    break;
                case "DuckDuckGo":
                    window.open("https://duckduckgo.com?q=" + searchContent);
                    break;
                default:
                    break;
            }
        } else {
            if (!captchaStatus) {
                handleOpenAlert();
            } else {
                console.log("captha failed");
            }
        }
    };
    return (
        <ThemeProvider theme={darkTheme}>
            <CssBaseline />

            <div className="wrapper">
                <Grid
                    container
                    className={classes.root}
                    direction="row"
                    justify="center"
                    alignItems="center"
                >
                    <Grid item xs={12} md={6}>
                        <FormControl
                            component="fieldset"
                            className={classes.formControl}
                        >
                            <FormLabel component="legend">
                                Search Engine
                            </FormLabel>
                            <RadioGroup
                                aria-label="searchEngine"
                                name="engine1"
                                value={searchEngine}
                                onChange={handleChangeEngine}
                            >
                                <FormControlLabel
                                    value="Google"
                                    control={<Radio />}
                                    label="Google"
                                />
                                <FormControlLabel
                                    value="Bing"
                                    control={<Radio />}
                                    label="Bing"
                                />
                                <FormControlLabel
                                    value="DuckDuckGo"
                                    control={<Radio />}
                                    label="DuckDuckGo"
                                />
                            </RadioGroup>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <div className="searchbar">
                            <Grid
                                container
                                direction="row"
                                justify="center"
                                alignItems="center"
                            >
                                <Grid item xs={12} sm={10}>
                                    <div className="searchField">
                                        <TextField
                                            variant="filled"
                                            label="Search something"
                                            fullWidth
                                            value={searchContent}
                                            onChange={(e) =>
                                                setSearchContent(e.target.value)
                                            }
                                        />
                                    </div>
                                </Grid>
                                <Grid item xs={12} sm={2}>
                                    <div class="searchBtn">
                                        <Button
                                            color="primary"
                                            variant="contained"
                                            onClick={onSearch}
                                        >
                                            <SearchIcon />
                                        </Button>
                                    </div>
                                </Grid>
                                <Grid item xs={12} md="auto">
                                    <Tooltip title="Type the thing you want to search before checking this box!">
                                        <div>
                                            <ReCAPTCHA
                                                class="recaptchaField"
                                                onChange={changeCaptchaStatus}
                                                sitekey="6LcWMd4UAAAAAFNSTOQK5jbq3M9KlvAqsuE_fT2p"
                                                theme="dark"
                                            />
                                        </div>
                                    </Tooltip>
                                </Grid>
                            </Grid>
                        </div>
                    </Grid>
                </Grid>
                <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">
                        {"Please Verify you are a human"}
                    </DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Check the box under the search bar to Verify you are
                            a human.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary" autoFocus>
                            OK
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        </ThemeProvider>
    );
}

export default App;
